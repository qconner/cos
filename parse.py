#!/usr/bin/env python

import os
import re
import sys

"""generate CSV from scraping Texas Secretary of State website
"""

def parse():
    if len(sys.argv) == 2:
        fn = sys.argv[1]
    else:
        print "usage: %s filename_without_suffix"
        sys.exit(1)
    f = open(fn + '.txt', 'r')

    #State Representative District 99 (Tarrant)	Charlie Geren	REP	P.O.Box 1440 Fort Worth, TX 76101	Nominee for General Election

    lines = f.readlines()
    f.close()

    candidates = dict()
    for line in lines:
        s = line.rstrip()

        # grok
        #m = re.search('^State\s.+\d+\s\(\S+\)\s(.+)\s(REP|DEM)\s(.+)$', s)
        m = re.search('([^\t]+)\s(\d+)\s\(([^\t\)]+)\)\t([^\t]*)\t([^\t]*)\t([^\t]*)\t([^\t]*)', s)
        junk1 = m.group(1)
        district = m.group(2)
        county = m.group(3)
        name = m.group(4).replace('"', "'")
        party = m.group(5)
        address = m.group(6)
        status = m.group(7)

        c_key = str(district)+name
        c_val = { 'district': district, 'county': [county], 'name': name, 'party': party, 'address': address, 'status': status }
        # have we seen this Candidate in this District before?
        if c_key in candidates:
            # add new county
            candidates[c_key]['county'].append(county)
        else:
            # new candidate for this district
            candidates[c_key] = c_val

    f = open(fn + '.csv', 'w+')
    for c_key in candidates:
        c = candidates[c_key]
        f.write(c['district'] + ', "' +
                str(c['county']) + '", "' +
                c['name'] + '", "' +
                c['party'] + '", "' +
                c['address'] + '", "' +
                c['status'] + '"\n')
    f.close()

if __name__ == "__main__":
  parse()
